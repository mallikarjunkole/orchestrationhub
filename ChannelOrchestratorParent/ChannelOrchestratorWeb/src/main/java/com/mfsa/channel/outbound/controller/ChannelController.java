package com.mfsa.channel.outbound.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mfsa.channel.outbound.dto.TransactionStatusRequestDto;
import com.mfsa.channel.outbound.dto.TransactionStatusResponseDto;
import com.mfsa.channel.outbound.service.TransactionStatusService;
import com.mfsa.channel.util.ChannelOrchestratorResponseCodes;
import com.mfsa.channel.util.CommonConstant;
import com.mfsa.channel.util.CommonValidations;


@RestController
public class ChannelController 
{
	@Autowired
	TransactionStatusService transStatusService;
	
	private static final Logger LOGGER = Logger.getLogger(ChannelController.class);

	@RequestMapping(value = "/Testing", method = RequestMethod.POST)
	public String Testing()
	{
		return "Testing done by Developer...!!";	
	}
	
	@RequestMapping(value = "/getTransactionStatus", method = RequestMethod.POST, produces = "application/json")
	public TransactionStatusResponseDto getTransactionStatus(@RequestBody TransactionStatusRequestDto request)
	{
		LOGGER.debug("getTransactionStatus in getTransactionStatus function " + request);
		TransactionStatusResponseDto response = null;
		if (request == null) 
		{
			response = new TransactionStatusResponseDto();
			response.setStatusCode(ChannelOrchestratorResponseCodes.INVALID_REQUEST.getCode());
			response.setStatusMessage(ChannelOrchestratorResponseCodes.INVALID_REQUEST.getMessage());
		} 
		else
		{
			CommonValidations commonValidation = new CommonValidations();
			if(commonValidation.validateStringValues(request.getRefernceid()))
			{
				response = new TransactionStatusResponseDto();
				response.setStatusCode(ChannelOrchestratorResponseCodes.VALIDATION_ERROR.getCode());
				response.setStatusMessage(ChannelOrchestratorResponseCodes.VALIDATION_ERROR.getMessage()+ " : "+CommonConstant.INVALID_REFERENCEID);
			}
			else if (commonValidation.validateStringValues(request.getFinancialtransactionid()))
			{
				response = new TransactionStatusResponseDto();
				response.setStatusCode(ChannelOrchestratorResponseCodes.VALIDATION_ERROR.getCode());
				response.setStatusMessage(ChannelOrchestratorResponseCodes.VALIDATION_ERROR.getMessage()+ " : "+CommonConstant.INVALID_FINANCIALTRANSACTIONID);
			} 			 
			else if(commonValidation.validateStringValues(request.getIdentity()))
			{
				response = new TransactionStatusResponseDto();
				response.setStatusCode(ChannelOrchestratorResponseCodes.VALIDATION_ERROR.getCode());
				response.setStatusMessage(ChannelOrchestratorResponseCodes.VALIDATION_ERROR.getMessage()+ " : "+CommonConstant.INVALID_IDENTITY);
			}
			else if(commonValidation.validateGetTranStatusRequestValues(request.getRefernceid(), request.getFinancialtransactionid()))
			{
				response = new TransactionStatusResponseDto();
				response.setStatusCode(ChannelOrchestratorResponseCodes.VALIDATION_ERROR.getCode());
				response.setStatusMessage(ChannelOrchestratorResponseCodes.VALIDATION_ERROR.getMessage()+ " : "+CommonConstant.INVALID_GETTRANSTATUS_REQUEST);
			}
			else
			{
				response = transStatusService.getTransactionStatus(request);			
			}
			/*response = new TransactionStatusResponseDto();
			response.setFinancialTransactionId("financialTransactionId");
			response.setProviderTransactionId("providerTransactionId");
			response.setStatus("Completed");*/
		}
		return response;		
	}
}
