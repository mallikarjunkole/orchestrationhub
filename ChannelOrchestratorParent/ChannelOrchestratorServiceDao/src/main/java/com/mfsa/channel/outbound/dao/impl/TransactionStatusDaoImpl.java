package com.mfsa.channel.outbound.dao.impl;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.mfsa.channel.outbound.dao.TransactionStatusDao;
import com.mfsa.channel.outbound.dto.ResponseStatus;
import com.mfsa.channel.outbound.dto.TransactionStatusRequestDto;
import com.mfsa.channel.outbound.exception.DaoException;
import com.mfsa.channel.outbound.models.CoTransDetailsOutbound;
import com.mfsa.channel.util.ChannelOrchestratorResponseCodes;


@EnableTransactionManagement
@Repository("TransactionStatusDao")
public class TransactionStatusDaoImpl extends BaseDaoImpl implements TransactionStatusDao
{
	private static final Logger LOGGER = Logger.getLogger(TransactionStatusDaoImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;

	@Transactional
	public CoTransDetailsOutbound getTransactionStatusByReferenceId(String reference_id) throws DaoException
	{
		CoTransDetailsOutbound coTransDetailsOutbound=null;
		Session session = sessionFactory.getCurrentSession();
		
		try 
		{
			coTransDetailsOutbound = (CoTransDetailsOutbound) session.createQuery("From CoTransDetailsOutbound where referenceId=:referenceId").setParameter("referenceId", reference_id).uniqueResult();
		}
		catch(Exception e)
		{
			ResponseStatus status = new ResponseStatus();
			status.setStatusMessage(ChannelOrchestratorResponseCodes.ER220.getMessage());
			status.setStatusCode(ChannelOrchestratorResponseCodes.ER220.getCode());
			throw new DaoException(status);
		}
		return coTransDetailsOutbound;
		
	}

	@Transactional
	public CoTransDetailsOutbound checkExistingRecords(String refernceid, String financialtransactionid) throws DaoException 
	{

		CoTransDetailsOutbound coTransDetailsOutbound=null;
		Session session = sessionFactory.getCurrentSession();
		
		try 
		{
			System.out.println("Inside Try : refernceid : "+refernceid+" financialtransactionid : "+financialtransactionid);
			coTransDetailsOutbound = (CoTransDetailsOutbound) session.createQuery("From CoTransDetailsOutbound where referenceId=:referenceId or financialTransactionId=:financialTransactionId").setParameter("referenceId", refernceid).setParameter("financialTransactionId", financialtransactionid).uniqueResult();
		}
		catch(Exception e)
		{
			ResponseStatus status = new ResponseStatus();
			status.setStatusMessage(ChannelOrchestratorResponseCodes.ER220.getMessage());
			status.setStatusCode(ChannelOrchestratorResponseCodes.ER220.getCode());
			throw new DaoException(status);
		}
		System.out.println("Inside DaoImpl : "+coTransDetailsOutbound);
		return coTransDetailsOutbound;
		
	
	} 
	
	
}
