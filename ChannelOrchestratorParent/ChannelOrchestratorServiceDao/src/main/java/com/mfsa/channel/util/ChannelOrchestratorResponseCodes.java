package com.mfsa.channel.util;

public enum ChannelOrchestratorResponseCodes 
{
	VALIDATION_ERROR("ER206","Validation Error"),
	INVALID_REQUEST("INVALID_REQUEST", "Request cannot be null"),
	ER204("ER204","Exception while save data"),
	ER205("ER205","Exception while update"),
	ER220("ER220","Exception while getting TransactionStatus by referenceId or financialtransactionid");
	
	private String code;
	private String message;
	
	private ChannelOrchestratorResponseCodes(String code, String message)
	{
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
