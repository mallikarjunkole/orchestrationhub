package com.mfsa.channel.outbound.dao.impl;

import org.apache.log4j.Logger;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mfsa.channel.outbound.dao.BaseDao;
import com.mfsa.channel.outbound.dto.ResponseStatus;
import com.mfsa.channel.outbound.exception.DaoException;
import com.mfsa.channel.util.ResponseCodes;

public class BaseDaoImpl implements BaseDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	private static final Logger LOGGER = Logger.getLogger(BaseDaoImpl.class);

	@Transactional
	public boolean save(Object obj) throws DaoException {
		LOGGER.debug("Inside BaseDAO Save");
		
		boolean isSuccess = false;
		try {
			this.sessionFactory.getCurrentSession().save(obj);
			isSuccess = true;
		} catch (Exception e) { 
			
			  LOGGER.error("==>Exception thrown in BaseDaoImpl in save " + e);
			  ResponseStatus status = new ResponseStatus();
			 
			  status.setStatusCode(ResponseCodes.ER204.getCode());
			  status.setStatusMessage(ResponseCodes.ER204.getMessage());
			  
			  throw new DaoException(status);
			 
		}
		return isSuccess;
	}

	@Transactional
	public boolean saveOrUpdate(Object obj) throws DaoException {
		LOGGER.debug("Inside BaseDAO saveOrUpdate");
		boolean isSuccess = false;
		try {
			this.sessionFactory.getCurrentSession().saveOrUpdate(obj);
			isSuccess = true;
		} catch (Exception e) {
			
			LOGGER.error("Exception in BaseDaoImpl in saveOrUpdate " + e);
			throw new DaoException("");
		}
		return isSuccess;
	}

	@Transactional
	public boolean update(Object obj) throws DaoException {
		LOGGER.debug("Inside BaseDAO Update");
		System.out.println("Inside BaseDAO Update");
		boolean isSuccess = false;
		try {
			this.sessionFactory.getCurrentSession().update(obj);
			System.out.println("Update : ");
			isSuccess = true;
		} catch (Exception e)
		{
			  System.out.println("Exception Message :"+e);
			  LOGGER.error("==>Exception thrown in BaseDaoImpl in update" + e);
			  System.out.println("Exception Message :"+e);
			  ResponseStatus status = new ResponseStatus();
			  status.setStatusCode(ResponseCodes.ER205.getCode());
			  status.setStatusMessage(ResponseCodes.ER205.getMessage());
			  throw new DaoException(status);
		}
		return isSuccess;
	}

}
