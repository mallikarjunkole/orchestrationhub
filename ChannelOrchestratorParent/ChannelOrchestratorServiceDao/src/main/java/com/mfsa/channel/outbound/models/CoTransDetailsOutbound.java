package com.mfsa.channel.outbound.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "co_tx_details_outbound")
public class CoTransDetailsOutbound implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6836815356718870587L;
	
	
	@Id
	@GeneratedValue
	@Column(name = "details_outbound_id")
	private int detailsOutboundId;
	
	@Column(name = "reference_id")
	private String referenceId;
	
	@Column(name = "sender_msisdn")
	private String senderMsisdn;
	
	@Column(name = "sender_sp_name")
	private String senderSpName;
	
	@Column(name = "receiver_msisdn")
	private String receiverMsisdn;
	
	@Column(name = "receiver_sp_name")
	private String receiverSpName;
	
	@Column(name = "amount")
	private double amount;
	
	@Column(name = "currency")
	private String currency;
	
	@Column(name = "date_logged")
	private Date dateLogged;
	
	@Column(name = "sender_note")
	private String senderNote;
	
	@Column(name = "receiver_message")
	private String receiverMessage;
	
	@Column(name = "mfs_transaction_id")
	private String mfsTransactionId;
	
/*	@Column(name = "thirdparty_trans_id")
	private String thirdpartyTransId;*/
	
	@Column(name = "financial_transaction_id")
	private String financialTransactionId;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "status_code")
	private String statusCode;
	
	@Column(name = "status_message")
	private String statusMessage;

	public int getDetailsOutboundId() {
		return detailsOutboundId;
	}

	public void setDetailsOutboundId(int detailsOutboundId) {
		this.detailsOutboundId = detailsOutboundId;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getSenderMsisdn() {
		return senderMsisdn;
	}

	public void setSenderMsisdn(String senderMsisdn) {
		this.senderMsisdn = senderMsisdn;
	}

	public String getSenderSpName() {
		return senderSpName;
	}

	public void setSenderSpName(String senderSpName) {
		this.senderSpName = senderSpName;
	}

	public String getReceiverMsisdn() {
		return receiverMsisdn;
	}

	public void setReceiverMsisdn(String receiverMsisdn) {
		this.receiverMsisdn = receiverMsisdn;
	}

	public String getReceiverSpName() {
		return receiverSpName;
	}

	public void setReceiverSpName(String receiverSpName) {
		this.receiverSpName = receiverSpName;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	public String getSenderNote() {
		return senderNote;
	}

	public void setSenderNote(String senderNote) {
		this.senderNote = senderNote;
	}

	public String getReceiverMessage() {
		return receiverMessage;
	}

	public void setReceiverMessage(String receiverMessage) {
		this.receiverMessage = receiverMessage;
	}

	public String getMfsTransactionId() {
		return mfsTransactionId;
	}

	public void setMfsTransactionId(String mfsTransactionId) {
		this.mfsTransactionId = mfsTransactionId;
	}

/*	public String getThirdpartyTransId() {
		return thirdpartyTransId;
	}

	public void setThirdpartyTransId(String thirdpartyTransId) {
		this.thirdpartyTransId = thirdpartyTransId;
	}*/

	public String getFinancialTransactionId() {
		return financialTransactionId;
	}

	public void setFinancialTransactionId(String financialTransactionId) {
		this.financialTransactionId = financialTransactionId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	@Override
	public String toString() {
		return "CoTransDetailsOutbound [detailsOutboundId=" + detailsOutboundId + ", referenceId=" + referenceId
				+ ", senderMsisdn=" + senderMsisdn + ", senderSpName=" + senderSpName + ", receiverMsisdn="
				+ receiverMsisdn + ", receiverSpName=" + receiverSpName + ", amount=" + amount + ", currency="
				+ currency + ", dateLogged=" + dateLogged + ", senderNote=" + senderNote + ", receiverMessage="
				+ receiverMessage + ", mfsTransactionId=" + mfsTransactionId + ", financialTransactionId="
				+ financialTransactionId + ", status=" + status + ", statusCode=" + statusCode + ", statusMessage="
				+ statusMessage + "]";
	}
	
	
	
}
