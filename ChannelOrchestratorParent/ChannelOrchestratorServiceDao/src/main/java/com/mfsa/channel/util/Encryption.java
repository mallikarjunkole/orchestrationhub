package com.mfsa.channel.util;

import org.apache.commons.codec.binary.Base64;

public class Encryption {
	
	public String encryption(String counter, String username, String passwords) {

			StringBuilder stringBuilder = new StringBuilder();

			stringBuilder.append(counter);
			stringBuilder.append(":");
			stringBuilder.append(username);
			stringBuilder.append(":");
			stringBuilder.append(passwords);
		

		byte[] authEncBytes = Base64.encodeBase64(stringBuilder.toString().getBytes());
		String authStringEnc = new String(authEncBytes);
		return authStringEnc;

	}

}