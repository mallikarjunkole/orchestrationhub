package com.mfsa.channel.util;

public enum ResponseCodes {
	
	S200("200" , "OK"),
	ER204("204" ,"No Content"),
	ER400("400" , "Bad Request"),
	ER401("401" , "Unauthorized "),
	ER403("403" , "Forbidden "),
	ER404("404" , "Not Found"),
	ER405("405" , "Method Not allowed "),
	ER408("408" , "Request Timeout "),
	ER500("500" , "Internal server error"),
	ER501("501" , "Not implemented "),
	ER503("503","Service Unavailable "),
	
	ER201("ER201", "SYSTEM ERROR"), 
	ER202("ER202", "Transaction Status Internal Error"),
//	ER204("ER204", "Exception while save data"), 
	ER205("ER205", "Exception while update"),
	ER206("ER206", "Exception while getting last record by accessTokenId"),
	ER207("ER207", "Exception while processing your request, Please try again later."),
	ER208("ER208", "Getting error while fetching the record from the database"),
	ER210("ER210", "Duplicate Reference Id"), 
	ER214("ER214", "No record found against referenceid"), 

	INVALID_REQUEST("INVALID_REQUEST", "Request cannot be null"),
	REQUIRED_REFERENCE_ID("REQUIRED_REFERENCE_ID", "Reference Id is Mandatory."),
	REQUIRED_AMOUNT("REQUIRED_AMOUNT", "Provide correct amount"),
	REQUIRED_DESTINATION("REQUIRED_DESTINATION", "Provide correct destination"), 
	INVALID_REFERENCE_ID("INVALID_REFERENCE_ID", "Provide correct referenceid"), 
	INVALID_MSISDN("INVALID_MSISDN", "Provide correct msisdn") ;

	private String code;
	private String message;
	

	private ResponseCodes(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

}
