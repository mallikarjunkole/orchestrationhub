package com.mfsa.channel.outbound.dao;

import java.util.Map;

import com.mfsa.channel.outbound.exception.DaoException;

public interface SystemConfigDao {

	public Map<String, String> getConfigDetailsMap() throws DaoException;

}
