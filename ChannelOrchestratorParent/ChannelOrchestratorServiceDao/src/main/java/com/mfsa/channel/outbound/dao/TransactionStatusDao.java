package com.mfsa.channel.outbound.dao;

import com.mfsa.channel.outbound.exception.DaoException;
import com.mfsa.channel.outbound.models.CoTransDetailsOutbound;

public interface TransactionStatusDao extends BaseDao{

	CoTransDetailsOutbound getTransactionStatusByReferenceId(String refernceid) throws DaoException;

	CoTransDetailsOutbound checkExistingRecords(String refernceid, String financialtransactionid)throws DaoException;

	
	

}
