package com.mfsa.channel.outbound.dao.impl;

import java.util.HashMap;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.mfsa.channel.outbound.dao.SystemConfigDao;
import com.mfsa.channel.outbound.exception.DaoException;
import com.mfsa.channel.outbound.models.CoSystemConfig;

@EnableTransactionManagement
@Repository("SystemConfigDao")
public class SystemConfigDaoImpl implements SystemConfigDao {

	@Autowired
	private SessionFactory sessionFactory;

	private static final Logger LOGGER = Logger.getLogger(SystemConfigDaoImpl.class);

	@Transactional
	public Map<String, String> getConfigDetailsMap() throws DaoException
	{
		LOGGER.info("Inside  getConfigDetailsMap");
		Map<String, String> systemConfigMap = new HashMap<String, String>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "From CoSystemConfig";
		Query query = session.createQuery(hql);

		List<CoSystemConfig> systemConfig = query.list();

		if (systemConfig != null && !systemConfig.isEmpty()) {
			systemConfigMap = new HashMap<String, String>();
			for (CoSystemConfig systemConfiguration : systemConfig) {
				systemConfigMap.put(systemConfiguration.getConfigKey(), systemConfiguration.getConfigValue());
			}
		}
		if (systemConfigMap != null && !systemConfigMap.isEmpty()) {
			LOGGER.debug("getConfigDetailsMap response size -> " + systemConfigMap.size());
		}
	return systemConfigMap;
	}

}
