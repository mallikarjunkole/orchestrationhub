package com.mfsa.channel.outbound.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
@XmlRootElement(name = "gettransactionstatusresponse")
@XmlType(propOrder={"financialtransactionid","status","providertransactionid"})
public class TransactionStatusResponseDto 
{
	private String financialtransactionid;
	private String status;
	private String providertransactionid;
	private String statusCode;
	private String statusMessage;
	
	@XmlElement(name = "financialtransactionid")
	public String getFinancialtransactionid() {
		return financialtransactionid;
	}
	public void setFinancialtransactionid(String financialtransactionid) {
		this.financialtransactionid = financialtransactionid;
	}
	
	@XmlElement(name = "status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@XmlElement(name = "providertransactionid")
	public String getProvidertransactionid() {
		return providertransactionid;
	}
	public void setProvidertransactionid(String providertransactionid) {
		this.providertransactionid = providertransactionid;
	}
	
	@XmlTransient
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	@XmlTransient
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	
	
	
}
