package com.mfsa.channel.outbound.service;

import com.mfsa.channel.outbound.dto.TransactionStatusRequestDto;
import com.mfsa.channel.outbound.dto.TransactionStatusResponseDto;

public interface TransactionStatusService 
{

	TransactionStatusResponseDto getTransactionStatus(TransactionStatusRequestDto request);

}
