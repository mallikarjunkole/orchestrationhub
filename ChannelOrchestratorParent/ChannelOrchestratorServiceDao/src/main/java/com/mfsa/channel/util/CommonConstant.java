package com.mfsa.channel.util;

public class CommonConstant
{
	public static final String INVALID_REFERENCEID = "invalid referenceId";
	public static final String INVALID_FINANCIALTRANSACTIONID = "invalid financialTransactionId";
	public static final String INVALID_IDENTITY = "invalid identity";	
	public static final String INVALID_GETTRANSTATUS_REQUEST = "referenceId or financialTransactionId is mandatory.";	
	
	public static final String REFERENCE_ID = "reference_id"; 
	public static final String FINANCIALTRANSACTIONID = "financial_transaction_id";
	public static final String IDENTITY = "identity";
	public static final String TOKEN = "token";
	public static final String KYC_ENQUIRY = "kyc_enquiry";
	
	public static final String PORT = "port";
	public static final String HTTP_METHOD = "httpmethod";
	public static final String BASE_URL = "base_url";
	public static final String ISHTTPS = "ishttps";

	public static final String CONTENT_TYPE = "Content-type";
	public static final String APPLICATION_JSON = "application/json";
	public static final String COUNTER = "counter";
	public static final String USER_NAME = "user_name";
	public static final String PASSWORD = "password";
	

	public static final String TRANSFER_FLOOZ_URL = "transfer_flooz_url";
	public static final String TRANSFER_STATUS_URL = "status_endpoint";
	public static final String TRANSFER_FLOOZ_BASE_URL = "transfer_flooz_base_url";
	public static final String TEXT_XML = "application/xml";
	public static final String TRANSFER_FLOOZ_SERVICE = "Transfer Flooz service";
	public static final String GET_TRANSFER_STATUS_SERVICE = "Get Status Service";
	
	public static final String ACCOUNT_STATUS_SERVICE = "Account Status Service";
	public static final String INITIATE_REQUEST = "Initial request";
	public static final String INITIATE_REQUEST_TO_PARTNER = "Initiate request to partner";
	public static final String RESPONSE_FROM_PARTNER = "Response from partner";
	public static final Object ACCOUNT_STATUS_URL = "account_status_url";
	

	

}
