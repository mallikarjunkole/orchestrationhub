package com.mfsa.channel.outbound.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;


@JsonInclude(Include.NON_NULL)
@XmlRootElement(name = "gettransactionstatusrequest")
@XmlType(propOrder={"refernceid","financialtransactionid","identity"})
public class TransactionStatusRequestDto 
{
	private String refernceid;
	private String financialtransactionid;
	private String identity;
	
	@XmlElement(name = "refernceid")
	public String getRefernceid() {
		return refernceid;
	}
	public void setRefernceid(String refernceid) {
		this.refernceid = refernceid;
	}
	
	@XmlElement(name = "financialtransactionid")
	public String getFinancialtransactionid() {
		return financialtransactionid;
	}
	public void setFinancialtransactionid(String financialtransactionid) {
		this.financialtransactionid = financialtransactionid;
	}
	
	@XmlElement(name = "identity")
	public String getIdentity() {
		return identity;
	}
	public void setIdentity(String identity) {
		this.identity = identity;
	}
	
	@Override
	public String toString() {
		return "TransactionStatusRequestDto [refernceid=" + refernceid + ", financialtransactionid="
				+ financialtransactionid + ", identity=" + identity + "]";
	}
	

	
	
}
