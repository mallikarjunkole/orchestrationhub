package com.mfsa.channel.util;

public class CommonValidations {

	public boolean validateStringValues(String reqParamValue)
	{
		
		
		boolean validateResult = false;
//		if (reqParamValue == null || reqParamValue.equalsIgnoreCase(" ") || reqParamValue.equalsIgnoreCase("")||reqParamValue.equalsIgnoreCase("null"))
		if (reqParamValue == null || reqParamValue.equalsIgnoreCase("null"))
			validateResult = true;

		return validateResult;
	}

	public boolean validateAmountValues(Double amount)
	{
		boolean validateResult = false;
		if (amount == null || amount < 0)
			validateResult = true;
 
		return validateResult;
	}

	public boolean validateNumber(String number) 
	{
		boolean validateResult = false;
		if (number == null || !number.matches("[0-9]+"))
			validateResult = true;
		return validateResult;

	}
	
	public boolean validateGetTranStatusRequestValues(String reqParamValue, String reqParamValue1)
	{
		String para1 = "";
		String para2 = "";
		if(reqParamValue != null && reqParamValue1 != null)
		{
			 para1 = reqParamValue.trim();
			 para2 = reqParamValue1.trim();
		}
		
		boolean validateResult = false;
		if (para1 == null || para1.equalsIgnoreCase("null") || para2 == null ||  para2.equalsIgnoreCase("null"))
		{	
			validateResult = true;
		}/*else if(para1 == "" && para2 != "" || para1 != "" && para2 == "")
		{
			System.out.println("para1 : "+para1 + "para2 : "+para2);
			validateResult = false;
		}*/else if(para1 == "" && para2 == "" )
		{
			validateResult = true;
		}
		return validateResult;
	}
	
}
