package com.mfsa.channel.util;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;

public class XmlParsingService {

	private static final Logger LOGGER = Logger.getLogger(XmlParsingService.class);

	public String objectToXml(Object request) 
	{
		String xmlContent = null;
		try 
		{
			JAXBContext jaxb = JAXBContext.newInstance(request.getClass());

			Marshaller marshaller = jaxb.createMarshaller();

			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

			StringWriter sw = new StringWriter();

			marshaller.marshal(request, sw);

			xmlContent = sw.toString();

		} 
		catch (Exception e)
		{
			System.out.println("Exeption Error Message : "+e);
			LOGGER.error(e);
		}
		return xmlContent;
	}

	public Object xmlToObjectResponse(Object object, String response) {

		// System.out.println("in");
		JAXBContext jaxbContext = null;
		Object responseDto = null;
		try {
			jaxbContext = JAXBContext.newInstance(object.getClass());

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			// jaxbUnmarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");

			responseDto = (Object) jaxbUnmarshaller.unmarshal(new StringReader(response));

		} catch (JAXBException e) {
			LOGGER.error(e);
		}

		return responseDto;
	}

}
