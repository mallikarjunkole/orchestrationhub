package com.mfsa.channel.outbound.service.impl;

import java.util.Date;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfsa.channel.outbound.dao.TransactionStatusDao;
import com.mfsa.channel.outbound.dao.SystemConfigDao;
import com.mfsa.channel.outbound.dto.TransactionStatusRequestDto;
import com.mfsa.channel.outbound.dto.TransactionStatusResponseDto;
import com.mfsa.channel.outbound.exception.DaoException;
import com.mfsa.channel.outbound.models.CoTransDetailsOutbound;
import com.mfsa.channel.outbound.models.CoTransferDetailsLog;
import com.mfsa.channel.outbound.service.TransactionStatusService;
import com.mfsa.channel.util.CommonConstant;
import com.mfsa.channel.util.HttpsConnectionRequest;
import com.mfsa.channel.util.ResponseCodes;
import com.mfsa.channel.util.XmlParsingService;


@Service("TransactionStatusService")
public class TransactionStatusServiceImpl implements TransactionStatusService
{
	private static final Logger LOGGER = Logger.getLogger(TransactionStatusServiceImpl.class);
	
	@Autowired
	private TransactionStatusDao transStatusDao;
	
	@Autowired
	SystemConfigDao systemConfigDao;

	public TransactionStatusResponseDto getTransactionStatus(TransactionStatusRequestDto request) 
	{
		LOGGER.info("Inside getTransactionStatus function : "+request);
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		TransactionStatusResponseDto response = null;
		Map<String, String> configMap;
		XmlParsingService parse = new XmlParsingService();
		
		try {
			// get Config Details from DB
			configMap = systemConfigDao.getConfigDetailsMap();
			if (configMap == null) 
			{
				throw new Exception("No Config Details in DB");
			}
			
			LOGGER.info("getTransactionStatusServiceImpl in getTransactionStatus function " + request);


	//      check entry in db for 
	//		checkExistingRecords(request);
			CoTransDetailsOutbound existingRecords = transStatusDao.checkExistingRecords(request.getRefernceid(), request.getFinancialtransactionid());
			System.out.println("existingRecords : "+existingRecords);
			
			if(existingRecords == null)
			{
			
			StringBuffer TransStatusReq = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "\n");
			
			// Convert Request To Xml String
			String TransStatusRequest = parse.objectToXml(request);
			TransStatusReq.append(TransStatusRequest);
			
//			System.out.println("Xml Request : "+TransStatusReq);
			
			LOGGER.info("getTransactionStatusImpl in getTransactionStatus function XML request " + TransStatusRequest);
			
			
	//		  connectionRequest.setHttpmethodName(configMap.get(CommonConstant.HTTP_METHOD));
	//		  connectionRequest.setHttps(Boolean.parseBoolean(configMap.get(CommonConstant.ISHTTPS)));
	//		  connectionRequest.setPort(Integer.parseInt(configMap.get(CommonConstant.PORT))); 
	//		  connectionRequest.setServiceUrl(configMap.get(CommonConstant.BASE_URL) +(configMap.get(CommonConstant.KYC_ENQUIRY)));
			

			String serviceResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + "<gettransactionstatusresponse>\n" + "<financialtransactionid>1290523</financialtransactionid>\n"
					+ "<status>SUCCESSFUL</status><providertransactionid>2017572712188</providertransactionid>\n"+ "</gettransactionstatusresponse>";
			
//			System.out.println("Response Before parse : "+serviceResponse);
			LOGGER.info("getTransactionStatusServiceImpl in getTransactionStatus function " + serviceResponse);
			
			response = (TransactionStatusResponseDto) parse.xmlToObjectResponse(new TransactionStatusResponseDto(), serviceResponse);
			System.out.println("response : "+response);
			LOGGER.info("KycEnquiryServiceImpl in kycEnquiry function " + response);
			
			
			if (response != null) 
				{
					logTransactionStatusResponse(request, response);
				} 
				else 
				{
					response = new TransactionStatusResponseDto();
					response.setStatusCode(ResponseCodes.ER202.getCode());
					response.setStatusMessage(ResponseCodes.ER202.getMessage());
				}
			}
			else if(existingRecords != null)
			{
				updateExistingTransactionStatusService(request, response);
				TransactionStatusResponseDto res = new TransactionStatusResponseDto();
				res.setFinancialtransactionid(existingRecords.getFinancialTransactionId());
				res.setStatus(existingRecords.getStatus());
				res.setProvidertransactionid(existingRecords.getMfsTransactionId());
				return res;
			}
			} 
			catch (DaoException de)
			{
				LOGGER.error("==>DaoException in getTransactionStatus" + de);
				response = new TransactionStatusResponseDto();
				response.setStatusCode(de.getStatus().getStatusCode());
				response.setStatusMessage(de.getStatus().getStatusMessage());
			} 
			catch (Exception e) 
			{
				LOGGER.error("==>Exception in getTransactionStatus" + e);
				response = new TransactionStatusResponseDto();
				response.setStatusCode(ResponseCodes.ER201.getCode());
				response.setStatusMessage(ResponseCodes.ER201.getMessage());
			}
		
		
		return response;
	}


	
	private void updateExistingTransactionStatusService(TransactionStatusRequestDto request, TransactionStatusResponseDto response) throws DaoException
	{
		System.out.println("updateTransactionStatusService");
		CoTransDetailsOutbound records = transStatusDao.checkExistingRecords(request.getRefernceid(), request.getFinancialtransactionid());
		System.out.println("Records : "+records);
		if(records != null)
		{	
			System.out.println("Inside if statement- updateTransactionStatusService");
			System.out.println("Reference id : "+request.getRefernceid());
			
			records.setReferenceId(request.getRefernceid());					
			records.setDateLogged(new Date());
			transStatusDao.update(records);
			System.out.println("After update");
		}
	}



	private void logTransactionStatusResponse(TransactionStatusRequestDto request,TransactionStatusResponseDto response) throws DaoException
	{
		LOGGER.debug("TransactionStatusResponse function in OutboundServiceImpl " + response);

		CoTransDetailsOutbound TransDetails = transStatusDao.getTransactionStatusByReferenceId(request.getRefernceid());

		if (TransDetails == null) 
		{
			CoTransDetailsOutbound outboundTransDetail = new CoTransDetailsOutbound();
			outboundTransDetail.setReferenceId(request.getRefernceid());
			outboundTransDetail.setSenderMsisdn(request.getIdentity());
			
			outboundTransDetail.setFinancialTransactionId(response.getFinancialtransactionid());
			outboundTransDetail.setMfsTransactionId(response.getProvidertransactionid());
			outboundTransDetail.setStatus(response.getStatus());
			outboundTransDetail.setDateLogged(new Date());
			
			transStatusDao.save(outboundTransDetail);
		} 	
		else 
		{
			TransDetails.setReferenceId(request.getRefernceid());
			TransDetails.setFinancialTransactionId(response.getFinancialtransactionid());
			TransDetails.setMfsTransactionId(response.getProvidertransactionid());
			TransDetails.setStatus(response.getStatus());
			
			TransDetails.setDateLogged(new Date());
			transStatusDao.update(TransDetails);
		}
			
	}



	
}
