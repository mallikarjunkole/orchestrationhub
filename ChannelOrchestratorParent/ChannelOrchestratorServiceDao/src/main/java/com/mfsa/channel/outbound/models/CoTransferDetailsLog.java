package com.mfsa.channel.outbound.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "co_transfer_details_log")
public class CoTransferDetailsLog implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5325976476755098993L;

	@Id
	@GeneratedValue
	@Column(name = "details_log_id")
	private int detailsLogId;
	
	@Column(name = "reference_id")
	private String referenceId;
	
	@Column(name = "sending_fri")
	private String sendingFri;
	
	@Column(name = "receiving_fri")
	private String receivingFri;
	
	@Column(name = "amount")
	private double amount;
	
	@Column(name = "currency")
	private String currency;
	
	@Column(name = "date_logged")
	private Date dateLogged;
	
	@Column(name = "sender_note")
	private String senderNote;
	
	@Column(name = "receiver_message")
	private String receiverMessage;
	
	@Column(name = "provider_transaction_id")
	private String providerTransactionId;
	
	@Column(name = "transaction_id")
	private String transactionId;
	
	@Column(name = "financial_transaction_id")
	private String financialTransactionId;
	
	@Column(name = "identity")
	private String identity;
	
	@Column(name = "status")
	private String status;

	public int getDetailsLogId() {
		return detailsLogId;
	}

	public void setDetailsLogId(int detailsLogId) {
		this.detailsLogId = detailsLogId;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getSendingFri() {
		return sendingFri;
	}

	public void setSendingFri(String sendingFri) {
		this.sendingFri = sendingFri;
	}

	public String getReceivingFri() {
		return receivingFri;
	}

	public void setReceivingFri(String receivingFri) {
		this.receivingFri = receivingFri;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	public String getSenderNote() {
		return senderNote;
	}

	public void setSenderNote(String senderNote) {
		this.senderNote = senderNote;
	}

	public String getReceiverMessage() {
		return receiverMessage;
	}

	public void setReceiverMessage(String receiverMessage) {
		this.receiverMessage = receiverMessage;
	}

	public String getProviderTransactionId() {
		return providerTransactionId;
	}

	public void setProviderTransactionId(String providerTransactionId) {
		this.providerTransactionId = providerTransactionId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getFinancialTransactionId() {
		return financialTransactionId;
	}

	public void setFinancialTransactionId(String financialTransactionId) {
		this.financialTransactionId = financialTransactionId;
	}

	public String getIdentity() {
		return identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "CoTransferDetailsLog [detailsLogId=" + detailsLogId + ", referenceId=" + referenceId + ", sendingFri="
				+ sendingFri + ", receivingFri=" + receivingFri + ", amount=" + amount + ", currency=" + currency
				+ ", dateLogged=" + dateLogged + ", senderNote=" + senderNote + ", receiverMessage=" + receiverMessage
				+ ", providerTransactionId=" + providerTransactionId + ", transactionId=" + transactionId
				+ ", financialTransactionId=" + financialTransactionId + ", identity=" + identity + ", status=" + status
				+ "]";
	}
	
	

}
